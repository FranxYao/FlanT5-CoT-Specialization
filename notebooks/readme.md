Notebooks used during developing this project, mostly data processing and visualization. 

Notebooks for inspecting the processed data
* `inspect_processed_data.ipynb`: an example about how `in-context chain-of-thought` data looks like.

Notebooks for visualization
* `dev_align_codex_to_flan_t5_dtw.ipynb`: notebook for aligning codex and flan t5 tokenized outputs using dynamic time warping
* `dev_process_codex_outputs.ipynb`: visualize the output probability of codex
* `dev_process_flan_t5_outputs.ipynb`: visualize the output probability of codex

Notebooks for prompting FlanT5
* `flan_t5_3b_asdiv.ipynb`: prompting FlanT5 3B on ASDIV dataset 
* `flan_t5_3b_gsm8k.ipynb`: prompting FlanT5 3B on GSM8K dataset
* `flan_t5_3b_multiarith.ipynb`: prompting FlanT5 3B on MultiArith dataset
* `flan_t5_3b_svamp.ipynb`: prompting FlanT5 3B on SVAMP dataset
* `flan_t5_11b_GSM8K.ipynb`: prompting FlanT5 11B on GSM8K dataset

The folder `archive` only serves for archive purpose. Notebooks in them may not be runable/ useful 